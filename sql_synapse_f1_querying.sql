-- DOMINANT_CONSTRUCTORS
SELECT TOP 10 c.name AS const_names, c.nationality, SUM(cs.wins) AS total_wins
FROM constructors AS c
INNER JOIN constructors_standings AS cs 
ON cs.constructorId = c.constructorId
GROUP BY c.name, c.nationality
ORDER BY total_wins DESC
-- DOMINANT_DRIVERS
SELECT TOP 10 d.forename, d.nationality, SUM(ds.wins) as total_wins
FROM drivers AS d
INNER JOIN driver_standings as ds 
ON d.driverId = ds.driverId 
GROUP BY d.forename, d.nationality
ORDER BY total_wins DESC;
-- MOST_RACES_HELD_IN_COUNTRIES
SELECT TOP 10 c1.country, c1.location, COUNT(DISTINCT r1.raceId) AS race_count
FROM circuits AS c1
INNER JOIN races AS r1 ON c1.circuitId = r1.circuitId
INNER JOIN results AS r2 ON r1.raceId = r2.raceId
INNER JOIN driver_standings AS d ON r2.driverId = d.driverId
GROUP BY c1.country, c1.location
ORDER BY race_count DESC;
-- MOST_RACES_HELD_ON_TRACKS
SELECT TOP 10 c1.name, c1.country, SUM(d.wins) AS total_wins 
FROM circuits AS c1
INNER JOIN races AS r1 ON c1.circuitId = r1.circuitId
INNER JOIN results AS r2 ON r1.raceId = r2.raceId
INNER JOIN driver_standings AS d ON r2.driverId = d.driverId
GROUP BY c1.name, c1.country
ORDER BY total_wins DESC;
-- LEWIS_HAMILTON_PERFORMANCE
SELECT d.forename, d.surname, rc.year, SUM(ds.points) as total_points
FROM drivers AS d
INNER JOIN driver_standings AS ds ON d.driverId = ds.driverId
INNER JOIN races AS rc ON ds.raceId = rc.raceId
WHERE d.forename = 'Lewis' AND d.surname = 'Hamilton'
GROUP BY d.forename, d.surname, rc.year
ORDER BY rc.year ASC;
-- JEDDAH_CIRCUIT_QUERY
SELECT c.circuitRef AS circuit_name, cc.name AS constructor_name, SUM(cr.points) AS total_points
FROM races AS r
INNER JOIN circuits AS c ON c.circuitId = r.circuitId
INNER JOIN constructors_standings AS cr ON cr.raceId = r.raceId
INNER JOIN constructors AS cc ON cr.constructorId = cc.constructorId
WHERE c.location = 'Jeddah'
GROUP BY cc.name, c.circuitRef
ORDER BY total_points DESC;



